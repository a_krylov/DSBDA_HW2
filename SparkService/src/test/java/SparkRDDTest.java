import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Before;
import org.junit.Test;

import service.SparkRDDService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SparkRDDTest {
    SparkRDDService sparkRDDService;

    @Before
    public void setUp() {
        sparkRDDService = new SparkRDDService();
        sparkRDDService.connect();
    }

    @Test
    public void testCountFlightEquallyAirport() {
        List<String> stringsInput = new ArrayList<>();
        stringsInput.add("A0001, 2020-01-23 15:14, Аэропорт Гибралтар, Аэропорт Голуэй");
        stringsInput.add("A0001, 2020-01-23 15:24, Аэропорт Гибралтар, Аэропорт Голуэй");
        stringsInput.add("A0001, 2020-01-23 16:24, Аэропорт Гибралтар, Аэропорт Голуэй");
        stringsInput.add("A0001, 2020-02-23 15:24, Аэропорт Гибралтар, Аэропорт Голуэй");
        JavaRDD<String> input = JavaSparkContext.fromSparkContext(sparkRDDService.getSparkContext())
                .parallelize(stringsInput);

        List<String> stringsCatalogue = new ArrayList<>();
        stringsCatalogue.add("Аэропорт Гибралтар - Великобритания");
        stringsCatalogue.add("Аэропорт Голуэй - Ирландия");
        JavaRDD<String> catalogue = JavaSparkContext.fromSparkContext(sparkRDDService.getSparkContext())
                .parallelize(stringsCatalogue);

        Map<String, Long> map = sparkRDDService.countFlightCountry(input, catalogue);
        Map<String, Long> map2 = new HashMap<>();
        map2.put("2020-01-23 15:00, Великобритания, Ирландия", 2L);
        map2.put("2020-01-23 16:00, Великобритания, Ирландия", 1L);
        map2.put("2020-02-23 15:00, Великобритания, Ирландия", 1L);

        assertEquals(map2, map);
    }

    @Test
    public void testCountFlightEquallyCountry() {
        List<String> stringsInput = new ArrayList<>();
        stringsInput.add("A0001, 2020-01-23 15:14, Аэропорт Гибралтар, Аэропорт Голуэй");
        stringsInput.add("A0001, 2020-01-23 15:17, Аэропорт Глазго Прествик, Аэропорт Голуэй");
        stringsInput.add("A0001, 2020-01-23 15:47, Аэропорт Глазго Прествик, Аэропорт Дублин");
        stringsInput.add("A0001, 2020-01-23 15:18, Аэропорт Гибралтар, Аэропорт Дублин");
        stringsInput.add("A0001, 2020-01-23 16:18, Аэропорт Гибралтар, Аэропорт Дублин");
        stringsInput.add("A0001, 2020-02-23 15:18, Аэропорт Гибралтар, Аэропорт Дублин");
        JavaRDD<String> input = JavaSparkContext.fromSparkContext(sparkRDDService.getSparkContext())
                .parallelize(stringsInput);

        List<String> stringsCatalogue = new ArrayList<>();
        stringsCatalogue.add("Аэропорт Гибралтар - Великобритания");
        stringsCatalogue.add("Аэропорт Глазго Прествик - Великобритания");
        stringsCatalogue.add("Аэропорт Голуэй - Ирландия");
        stringsCatalogue.add("Аэропорт Дублин - Ирландия");
        JavaRDD<String> catalogue = JavaSparkContext.fromSparkContext(sparkRDDService.getSparkContext())
                .parallelize(stringsCatalogue);

        Map<String, Long> map = sparkRDDService.countFlightCountry(input, catalogue);
        Map<String, Long> map2 = new HashMap<>();
        map2.put("2020-01-23 15:00, Великобритания, Ирландия", 4L);
        map2.put("2020-01-23 16:00, Великобритания, Ирландия", 1L);
        map2.put("2020-02-23 15:00, Великобритания, Ирландия", 1L);

        assertEquals(map2, map);
    }

    @Test
    public void testCountFlight() {
        List<String> stringsInput = new ArrayList<>();
        stringsInput.add("A0001, 2020-01-23 15:14, Аэропорт Гибралтар, Аэропорт Голуэй");
        stringsInput.add("A0001, 2020-01-23 15:17, Аэропорт Гибралтар, Аэропорт Задар");
        stringsInput.add("A0001, 2020-01-23 15:17, Аэропорт Задар, Аэропорт Гибралтар");
        stringsInput.add("A0001, 2020-01-23 15:56, Аэропорт Будапешт Ференц Лист, Аэропорт Берн Бельп");
        JavaRDD<String> input = JavaSparkContext.fromSparkContext(sparkRDDService.getSparkContext())
                .parallelize(stringsInput);

        List<String> stringsCatalogue = new ArrayList<>();
        stringsCatalogue.add("Аэропорт Гибралтар - Великобритания");
        stringsCatalogue.add("Аэропорт Голуэй - Ирландия");
        stringsCatalogue.add("Аэропорт Задар - Хорватия");
        stringsCatalogue.add("Аэропорт Будапешт Ференц Лист - Венгрия");
        stringsCatalogue.add("Аэропорт Берн Бельп - Швейцария");
        JavaRDD<String> catalogue = JavaSparkContext.fromSparkContext(sparkRDDService.getSparkContext())
                .parallelize(stringsCatalogue);

        Map<String, Long> map = sparkRDDService.countFlightCountry(input, catalogue);
        Map<String, Long> map2 = new HashMap<>();
        map2.put("2020-01-23 15:00, Великобритания, Ирландия", 1L);
        map2.put("2020-01-23 15:00, Великобритания, Хорватия", 1L);
        map2.put("2020-01-23 15:00, Хорватия, Великобритания", 1L);
        map2.put("2020-01-23 15:00, Венгрия, Швейцария", 1L);

        assertEquals(map2, map);
    }
}