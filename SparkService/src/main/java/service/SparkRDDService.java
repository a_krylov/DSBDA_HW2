package service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Getter
public class SparkRDDService {
    private SparkContext sparkContext;

    /**
     * Метод инициализации необходимых параметров
     */
    public void connect() {

        SparkConf sparkConf = new SparkConf()
                .setAppName("Flight")
                .setMaster("local")
                .set("spark.driver.host", "127.0.0.1");

        sparkContext = SparkContext.getOrCreate(sparkConf);
    }

    /**
     * Получаем данные из файлов.
     * Для генератора формат входных данных Аэропорт-Страна. Происходит парсинг справочника в мапу.
     *
     * Входной файл наполнен строками типа
     * A0001, 2020-01-23 15:14, Аэропорт Гибралтар, Аэропорт Голуэй
     *
     * Возвращаем словарь.
     * Ключ - является связка вида: Дата вылета, Страна вылета, Страна прилета
     * Значение - количество таких данных
     */
    public Map<String, Long> countFlightCountry(JavaRDD<String> inputJavaRDD, JavaRDD<String> catalogueJavaRDD) {
        Map<String, String> catalogue = new HashMap<>();
        catalogueJavaRDD.collect().forEach(record -> {
            String[] stringsCatalogue = record.split(" - ");
            catalogue.put(stringsCatalogue[0], stringsCatalogue[1]);
        });

        inputJavaRDD = inputJavaRDD
                .map(record ->
                {
                    String[] strings = record.split(", ");
                    return String.join(", ", strings[1].substring(0, 14) + "00",
                            catalogue.get(strings[2]), catalogue.get(strings[3]));
                });

        return inputJavaRDD.countByValue();
    }
}
