package service;


import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.UUID;


@Slf4j
public class CassandraService {
    private CqlSession session;

    /**
     * Метод инициализации необходимых параметров
     * Создание пространства java_api, если отсутсвтвует
     * Создание заново таблицы flight
     */
    public void connect() {
        session = CqlSession.builder()
                .build();

        session.execute("DROP KEYSPACE IF EXISTS java_api");
        session.execute("CREATE KEYSPACE java_api WITH replication= {'class': 'SimpleStrategy', 'replication_factor': 1}");
        session.execute("CREATE TABLE java_api.flight (id UUID PRIMARY KEY,flight_time TEXT, country_from TEXT, country_to TEXT, flight_count BIGINT)");
    }

    public void close() {
        session.close();
    }

    /**
     * Запись одного значения в таблицу flight
     */
    private void insertFlight(String flightTime, String countryFrom, String countryTo, long flightCount) {
        session.execute("INSERT INTO java_api.flight (id, flight_time, country_from, country_to, flight_count) VALUES (?, ?, ?, ?, ?)",
                UUID.randomUUID(), flightTime, countryFrom, countryTo, flightCount);
    }

    /**
     * Обработка всех значений полученных SparkRDD
     */
    public void insertAllFlight(Map<String, Long> map) {
        for (Map.Entry entry : map.entrySet()) {
            String[] strings = entry.getKey().toString().split(",");
            insertFlight(strings[0], strings[1], strings[2], (Long) entry.getValue());
        }
    }

    /**
     * Вывод в логи всех значений из таблицы java_api.flight
     */
    public void showAllFlight() {
        ResultSet result = session.execute("select * from java_api.flight");
        result.all().forEach(row -> log.info(toString(row)));
    }

    private String toString(Row row) {
        String result;
        result = "flight_time: " + row.getString("flight_time") +
                ", country_from: " + row.getString("country_from") +
                ", country_to: " + row.getString("country_to") +
                ", flight_count: " + row.getLong("flight_count");
        return result;
    }

}