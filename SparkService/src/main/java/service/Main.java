package service;

import lombok.extern.log4j.Log4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

@Log4j
public class Main {
    public static void main(String[] args) {
        SparkRDDService sparkRDD = new SparkRDDService();
        CassandraService cassandraService = new CassandraService();
        String pathInputData = "/root/DSBDA_HW2/input.txt";
        String pathCatalogue = "/root/DSBDA_HW2/catalogue.txt";

        log.info("=====START SPARK=====");
        sparkRDD.connect();
        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkRDD.getSparkContext());
        JavaRDD<String> inputData = javaSparkContext.textFile(pathInputData);
        JavaRDD<String> catalogue = javaSparkContext.textFile(pathCatalogue);

        log.info("=====START CASSANDRA=====");
        cassandraService.connect();

        log.info("=====START SERVICE=====");
        cassandraService.insertAllFlight(sparkRDD.countFlightCountry(inputData, catalogue));

        log.info("=====RESULT OF WORK=====");
        cassandraService.showAllFlight();

        cassandraService.close();
    }

}
