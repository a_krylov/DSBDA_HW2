import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;

import static java.time.temporal.ChronoField.YEAR;

public class Generator {
    /**
     * Генератор создает файлы в директории DSDBA_HW2.
     * В файл catalogue.txt генерируется 15 строк с парой значений Аэропорт-Страна.
     * Всего 15 рандомных аэропртов из 10 стран.
     * <p>
     * В файл input.txt генерируется 500 строк.
     * Каждая строка создается с рандомными параметрами:
     * -raceNumber - первые 5 символов UUID'a
     * -time - время вылета в формате yyyy-MM-dd HH:mm, берется рандомное время в минутах от текущего времени + сутки.
     * -airportOut аэропорт вылета, рандомный из списка в каталоге
     * -airportIn аэропорт прилета, рандомный из списка в каталоге
     */
    public static void main(String[] args) {
        String pathInputData = "input.txt";
        String pathCatalogue = "catalogue.txt";
        int numberAirports = 15;
        int numberCountries = 10;
        int numberLines = 500;

        String[] countries = new String[]{"Spain", "England", "Portugal", "France", "Germany", "Croatia", "Hungary", "Austria", "USA", "Canada"};

        //Генерация аэропортов и связок Аэропорт - Страна
        List<String> airports = new ArrayList<>(numberAirports);
        Map<String, String> airportsCountries = new HashMap<>();
        for (int i = 0; i < numberAirports; i++) {
            airports.add(UUID.randomUUID().toString().substring(0, 3));
            airportsCountries.put(airports.get(i), countries[randomInt(0, numberCountries - 1)]);
        }

        // Идет запись в catalogue.txt
        PrintStream fileCatalogue;
        try {
            fileCatalogue = new PrintStream(pathCatalogue);
            for (int j = 0; j < numberAirports; j++) {
                String raw = String.join(" - ", airports.get(j), airportsCountries.get(airports.get(j)));
                fileCatalogue.println(raw);
            }
            fileCatalogue.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Идет запись в input.txt
        PrintStream fileInput;
        try {
            fileInput = new PrintStream(pathInputData);
            for (int j = 0; j < numberLines; j++) {
                String raceNumber = UUID.randomUUID().toString().substring(0, 5);
                String time = LocalDateTime.now().plusMinutes(randomInt(0, 1440)).format(formatter);
                String airportOut = airports.get(randomInt(0, numberAirports - 1));
                String airportIn = airports.get(randomInt(0, numberAirports - 1));
                String flight = String.join(", ", raceNumber, time, airportOut, airportIn);
                fileInput.println(flight);
            }
            fileInput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static int randomInt(int min, int max) {
        return (int) (Math.random() * (max - min + 1) + min);
    }

    private static DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd HH:mm")
            .toFormatter();


}
